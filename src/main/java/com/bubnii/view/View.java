package com.bubnii.view;

import com.bubnii.controller.Controller;
import com.bubnii.interfaces.Printable;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {
    private Controller controller = new Controller();
    private MessagePrinter print = new MessagePrinter();
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Scanner input;

    public View() {
        input = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "\t1 - Show some synchronized lock/unlock methods");
        menu.put("2", "\t2 - Show use of BlockingQueue");
        menu.put("3", "\t3 - Show Fibonacci numbers of executors");
        menu.put("Q", "\tQ - exit");
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::showPlayPingPong);
        methodsMenu.put("2", this::showFibonacciNumberCreation);
        methodsMenu.put("3", this::showFibonacciExecutors);
;
    }

    private void showPlayPingPong() {
        controller.runSomeSynchronizedMethods();
    }

    private void showFibonacciNumberCreation() {
        controller.runUseBlockingQueue();
    }
    private void showFibonacciExecutors() {
//        controller.runFibonacciExecutors();
    }

    public void show() {
        String keyMenu;
        do {
            print.printMessage("------------------------------------------------------");
            outputMenu();
            print.printMessage("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            quitProgram(keyMenu);
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } while (true);
    }

    private void quitProgram(String keyMenu) {
        if (keyMenu.equals("Q")) {
            System.exit(0);
        }
    }

    private void outputMenu() {
        print.printMessage("MENU: Java Concurrency");
        for (String str : menu.values()) {
            print.printMessage(str);
        }
    }
}
