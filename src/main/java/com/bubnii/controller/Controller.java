package com.bubnii.controller;

import com.bubnii.model.BlockingQueueExample;
import com.bubnii.model.SynchronizeStart;

public class Controller {

    SynchronizeStart synchronizeStart;
    BlockingQueueExample blockingQueue;

    public Controller() {
        this.synchronizeStart = new SynchronizeStart();
        this.blockingQueue = new BlockingQueueExample();
    }

    public void runSomeSynchronizedMethods() {
        synchronizeStart.someSynchronizedMethods();
    }

    public void runUseBlockingQueue(){
        blockingQueue.useBlockingQueue();
    }
}
