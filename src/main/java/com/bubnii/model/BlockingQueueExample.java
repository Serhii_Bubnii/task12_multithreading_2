package com.bubnii.model;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

public class BlockingQueueExample {

    private ArrayBlockingQueue<String> blockingQueue = new ArrayBlockingQueue<>(2, true);

    public void useBlockingQueue() {

        final String[] messages = {"Modify", "exercise" ,"seven", "from", "previous", "presentation",
                "to", "use", "a", "BlockingQueue", "instead", "of", "a", "pipe."};
        final String DONE = "done";

        Thread blockingQueuePut = new Thread(() -> {
            try {
                for (int i = 0; i < messages.length; i++) {
                    blockingQueue.put(messages[i]);
                    System.out.println("Queue Put: " + messages[i]);
                    TimeUnit.MILLISECONDS.sleep(1000);
                }
                blockingQueue.put(DONE);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        Thread blockingQueueTake = new Thread(() -> {
            try {
                TimeUnit.SECONDS.sleep(5);
                String msg;
                while (!((msg = blockingQueue.take()).equals(DONE))) {
                    System.out.println("Queue take: " + msg);
                    TimeUnit.MILLISECONDS.sleep(500);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        blockingQueuePut.start();
        blockingQueueTake.start();
    }
}
