package com.bubnii.model;

public class SynchronizeStart {

    SynchronizeOnLock onLock = new SynchronizeOnLock();

    public void someSynchronizedMethods() {
        Thread thread1 = new Thread(onLock::methodOne);
        Thread thread2 = new Thread(onLock::methodTwo);
        Thread thread3 = new Thread(onLock::methodThree);

        thread1.start();
        thread2.start();
        thread3.start();

        try {
            thread1.join();
            thread2.join();
            thread3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
