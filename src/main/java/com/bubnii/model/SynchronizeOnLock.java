package com.bubnii.model;

import com.bubnii.view.MessagePrinter;

import java.util.concurrent.locks.ReentrantLock;

public class SynchronizeOnLock {

    private ReentrantLock lock = new ReentrantLock();
    private MessagePrinter printer = new MessagePrinter();

    public void methodOne() {
        lock.lock();
        try {
            for (int i = 0; i < 5; i++) {
                printer.printMessage("Lock.methodOne");
            }
        } finally {
            lock.unlock();
        }
        printer.printMessage("finish " + Thread.currentThread().getName());
    }

    public void methodTwo() {
        lock.lock();
        try {
            for (int i = 0; i < 5; i++) {
                lock.lock();
                printer.printMessage("Lock.methodTwo");
                lock.unlock();
            }
        } finally {
            lock.unlock();
        }
        printer.printMessage("finish " + Thread.currentThread().getName());
    }

    public void methodThree() {
        lock.lock();
        try {
            for (int i = 0; i < 5; i++) {
                printer.printMessage("Lock.methodThree");
            }
        } finally {
            lock.lock();
        }
        printer.printMessage("finish " + Thread.currentThread().getName());
    }
}
